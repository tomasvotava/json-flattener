# pylint: disable=global-statement
import os
import sys
import json
import csv
from logging import getLogger, basicConfig, DEBUG, INFO
import argparse
from math import ceil

try:
    from jsonpath_rw import parse as json_parse
    jsonpath = True 
except ImportError:
    print("Starting without JSONPath support. If you need it, try:\npip install jsonpath-rw")
    jsonpath = False

PROGRESSCHAR = "▓"

aparser = argparse.ArgumentParser(sys.argv[0])
aparser.add_argument("--debug", action="store_true", default=False, help="Show more output")
aparser.add_argument("input", help="JSON input file to flatten")
aparser.add_argument("--output", default="./output", help="Output folder to hold output CSV files (default: ./output)")
aparser.add_argument("--no-progress", action="store_true", default=False, help="Do not display processing progress (makes things a little faster)")
aparser.add_argument("--encoding", action="store", default="utf-8", help="Encoding to use when opening and writing files (default: utf-8)")
aparser.add_argument("--jsonpath", action="store", default="$", help="JSONPath used to extract data root (e.h. '$.data', default: '$')")
args = aparser.parse_args(sys.argv[1:])

basicConfig(
    format="[{asctime}] [{levelname}]: {message}",
    style="{",
    level=DEBUG if args.debug else INFO
)

logger = getLogger(__name__)

logger.debug("Running in debug mode")

if not os.path.exists(args.output):
    logger.debug("Output folder %s does not exist, creating it", args.output)
    os.makedirs(args.output)
else:
    logger.warning("Output folder %s exists, any existing files may be overwritten", args.output)

if not os.path.exists(args.input):
    logger.critical("Input file %s does not exist", args.input)
    sys.exit(255)

logger.info("Loading input data...")

try:
    json_expr = json_parse(args.jsonpath)
except Exception as e:
    logger.critical("Could not parse specified JSONPath (%s)", str(e))
    sys.exit(120)

try:
    with open(args.input, "r", encoding=args.encoding) as infile:
        input_data = json.load(infile)
        data = json_expr.find(input_data)[0].value
except PermissionError as e:
    logger.critical("You don't have permissions to read input file %s", args.input)
    logger.critical(str(e))
    sys.exit(255)
except Exception as e:
    logger.critical("Unexpected error occurred")
    logger.critical(str(e))
    sys.exit(255)
    
logger.info("Input data contains %d rows", len(data))

flattened_objects = {}
root_objects = []

def progressbar(current, total, maxwidth=40, info=True):
    """Returns a nice console progressbar based on current state.

    Arguments:
        current {int} -- Current progress done
        total {total} -- Total progress to be made

    Keyword Arguments:
        maxwidth {int} -- Max width on the screen (default: {40})
        info {bool} -- Display textual info along with the progressbar
    """
    perc = current / total
    if info:
        infotext = " {} %".format(round(perc*100))
        maxwidth -= len(infotext)
    plen = int(ceil((maxwidth-3)*perc))
    percbar = (PROGRESSCHAR*plen) + (" "*(maxwidth-plen))
    pbar = "|{}|{}".format(percbar, infotext if info else "")
    return pbar

def iid(o_type):
    """Returns id of the current flattened object of a given type and increments the last inserted index

    Arguments:
        o_type {str} -- Flattened object name

    Returns:
        int -- Current insert id
    """
    global flattened_objects
    flattened_objects[o_type]["last_insert_id"] += 1
    return flattened_objects[o_type]["last_insert_id"]

def flatten(o, name, pid, parent_name="root"):
    """Flattens recursively current object

    Arguments:
        o {dict} or {list} -- Object to be flattened
        name {str} -- Flattened object name
        pid {int} -- Parent id
    """
    global flattened_objects
    if not name in flattened_objects:
        flattened_objects[name] = {"last_insert_id": 0, "data": []}
    current_id = iid(name)
    if isinstance(o, list):
        for li in o:
            if isinstance(li, list):
                flatten(li, "{}_{}".format(parent_name, name), current_id, name)
                continue
            current = {
                "__parent_id": pid,
                "__id": current_id
            }
            if isinstance(li, dict):
                for k, v in li.items():
                    if isinstance(v, (list, dict)):
                        flatten(v, "{}_{}".format(name, k), current_id, name)
                    else:
                        current[k] = v
            else:
                current[name] = li
            flattened_objects[name]["data"].append(current)
            current_id = iid(name)
    elif isinstance(o, dict):
        current = {
            "__parent_id": pid,
            "__id": current_id
        }
        for k, v in o.items():
            if isinstance(v, (dict, list)):
                flatten(v, "{}_{}".format(parent_name, name), current_id, name)
                continue
            current[k] = v
        flattened_objects[name]["data"].append(current)

logger.info("Flattening input data...")

for parent_id, parent in enumerate(data):
    if not args.no_progress and (parent_id % 10 == 0 or parent_id == len(data)-1):
        sys.stdout.write("\t{}\r".format(progressbar(parent_id+1, len(data))))
    current_object = {
        "__id": parent_id
    }
    for k, v in parent.items():
        if isinstance(v, (list, dict)):
            flatten(v, k, parent_id)
        else:
            current_object[k] = v
    root_objects.append(current_object)

if not args.no_progress:
    sys.stdout.write("\n")

logger.info("Exporting root object (%d rows)...", len(root_objects))

with open(os.path.join(args.output, "root.csv"), "w", encoding=args.encoding) as outfile:
    w = csv.DictWriter(outfile, fieldnames=root_objects[0].keys(), dialect=csv.unix_dialect)
    w.writeheader()
    w.writerows(root_objects)

logger.info("Exporting flattened objects...")

for fname, fobj in flattened_objects.items():
    logger.info("Exporting %s (%d)", fname, len(fobj["data"]))
    with open(os.path.join(args.output, "{}.csv".format(fname)), "w", encoding=args.encoding) as outfile:
        w = csv.DictWriter(outfile, fieldnames=fobj["data"][0].keys(), dialect=csv.unix_dialect)
        w.writeheader()
        w.writerows(fobj["data"])
