# JSON Flattener

```text
usage: .\flatten.py [-h] [--debug] [--output OUTPUT] [--no-progress]
                    [--encoding ENCODING] [--jsonpath JSONPATH]
                    input

positional arguments:
  input                JSON input file to flatten

optional arguments:
  -h, --help           show this help message and exit
  --debug              Show more output
  --output OUTPUT      Output folder to hold output CSV files (default: ./output)
  --no-progress        Do not display processing progress (makes things a little faster)
  --encoding ENCODING  Encoding to use when opening and writing files (default: utf-8)
  --jsonpath JSONPATH  JSONPath used to extract data root (e.h. '$.data', default: '$')
```
